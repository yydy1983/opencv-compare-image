const test = require('tape');
let cv = null;
test("Smoke tests / Can Import", function (t){
    cv = require('../lib/index');
    t.ok(cv, 'imported fine');
    t.ok(cv.version, "version is there:" + cv.version);
    t.end();
});

//Test The Examples folder
require('./examples')();